﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace NotasMusicaisFlyweight
{
    public class NotasMusicais
    {
        private static IDictionary<string, INota> notas = new Dictionary<string, INota>()
        {
            {  "do", new Do() },
            {  "re", new Re() },
            {  "mi", new Mi() },
            { "fa", new Fa() },
            { "sol", new Sol() },
            { "la", new La() },
            { "si", new Si() },
        };

        public INota GetNota(string nota)
        {
            if (notas.Keys.Contains(nota))
            {
                return notas[nota];
            }
            return notas["do"];
        }
    }
}

﻿using System;
using System.Collections.Generic;

namespace NotasMusicaisFlyweight
{
    public class Piano
    {
        public  void Play(IList<INota> musica)
        {
            foreach (var nota in musica)
            {
                Console.Beep(nota.Frequencia, 300);
            }
        }
    }
}

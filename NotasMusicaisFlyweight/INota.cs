﻿namespace NotasMusicaisFlyweight
{
    public interface INota
    {
        int Frequencia { get;  }
    }
}
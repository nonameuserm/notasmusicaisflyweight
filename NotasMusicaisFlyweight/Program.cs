﻿using System;
using System.Collections.Generic;

namespace NotasMusicaisFlyweight
{
    class Program
    {
        static void Main(string[] args)
        {
            NotasMusicais notas = new NotasMusicais();
            var musica = new List<INota>
            {
                notas.GetNota("do"), 
                notas.GetNota("re"),
                notas.GetNota("mi"),
                notas.GetNota("fa"),
                notas.GetNota("fa"),
                notas.GetNota("fa")
            };

            new Piano().Play(musica);
        }
    }
}
